This little Script is a simple Tracker for your personal Challenges (e.g. Do 100 Squats per day for 30 Days). 
Because it is written in Python it is formaly Platform independent (with Android I use Termux). 
I try to follow the Unix philosophy or KISS. This offer the  opportunity synchronization via a cloud service or git (for example). 

**Status:** Beta/Testing

**Known Bugs/Not implemented yet**

  * Remove Sessions/ Challenges
  * Start script with args to skip menue

**Dependencies:** python

**Usage:**

Start `challenge.py` and follow the Menue. 

At the first start the script run a initial setup. There you give a path to store the challenges progress.   
Datastructure after Setup:   
In the folder of the script you will find a `config.ini`. In this file you find the path for storing the progress -- lets name this path `USERPATH`.   
In this `USERPATH` the script writes a `Session.ini` with settings of all started challenges. In the same folder each challenge will create a file called `<Challenge-ID>.json`, 
where start-,enddate and the daily progress will be stored. 

**ToDo:** 

  * Comment the Source properly
  * see 'Known Bugs/Not implemented yet'
  * maybe a GUI or HTML5-Frontend
