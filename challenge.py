#!/usr/bin/python

from datetime import date, datetime, timedelta
import json
import configparser
import os


class Setup:
    def __init__(self, configsessions):
        self.configb = configparser.ConfigParser()
        self.configsessions = configsessions

    # The handler-function work with potentially multiple challenges. you can choose one, delete or shutdownthe
    # script. If no challenge found the hanlder call a function to create one. After initial-setup ist this the
    # 'mainmenue'.
    def handler(self):
        try:
            with open(self.configsessions, "r") as session:
                self.configb.read(self.configsessions)
                print("\nChoose one Challenge-ID:")
                # `options` just contains the sections of the parsed `Session.ini`
                options = self.configb.sections()
                # create a numbered list of each challenge and add basic commands
                for idx, element in enumerate(options):
                    print("{}) {}".format(idx + 1, element))
                print("x) Close")
                print("n) Start new Challenge")
                print("r) Remove Challenge")
                i = input("\nChoose a Option: ")
                if 0 < int(i) <= len(options):
                    chooseno = int(i) - 1
                    self.challengeid = options[chooseno]
                    # take Challenge-ID and call the Challenge-Class
                    chall = Challenge(self.challengeid)
                    chall.handler()
                elif i == "x":
                    # shutdown the script
                    raise SystemExit()
                elif i == "n":
                    # call the function that create a additional (or initial) new challenge
                    self.createnew()
                elif i == "r":
                    # remove a challenge. for saftyreasons the challenge-id must type in manually.
                    print("!ATTENTION! Action can not be undone!")
                    delsession = input("Type the Name of the Challenge you want to remove: \n")
                    # TODO test if delsession is a challengeid
                    self.removesession(delsession)
                else:
                    self.handler()
        # if no challenges/ini-file found then create one
        except OSError:
            # call the function that create a additional (or initial) new challenge
            self.createnew()

    # this function create a new challenge. this can be a additional challenge or the first/initial one.
    def createnew(self):
        self.challengeid = input("\nEnter new Challenge-ID: \n")
        self.configb[self.challengeid] = {}

        # inputs to get the values for the challenges
        while True:
            try:
                durday = int(input("How many days should the challenge last? "))
            except ValueError:
                print("Sorry, please give a Number.")
                continue
            else:
                break
        while True:
            try:
                reps = int(input("How many Repetitions? "))
            except ValueError:
                print("Sorry, please give a Number.")
                continue
            else:
                break
        print("Or do you prefer to count Sets instead? (If not pass the input)")
        # TODO count sets insted of Repetitions
        while True:
            try:
                sets = input("How many Sets instead of Repetitions? - [default: 0] \n")
                if len(sets) == 0:
                    sets = 0
                sets = int(sets)
            except ValueError:
                print("Sorry, please give a Number.")
                continue
            else:
                break

        # store the keys + values in the challengeid-section of the `Session.ini`
        self.configb[self.challengeid]['Duration'] = str(durday)
        self.configb[self.challengeid]['Repetitions'] = str(reps)
        self.configb[self.challengeid]['Sets'] = str(sets)
        # write the `Session.ini`
        with open(self.configsessions, "w") as session:
            self.configb.write(session)
        # call Challenge-Class with the new challenge
        Challenge(self.challengeid)

    def removesession(self, challengeid):
        pass
        # TODO: REMOVE SECTION/OPTON and json -> self.handler
        # https://docs.python.org/3.8/library/configparser.html#configparser.ConfigParser.remove_option


# this class take the values from the challenge-id-section of the `Session.ini` but mainly works with the
# <challenge-id>.json file witch contains start and end dates and progress values.
class Challenge:
    def __init__(self, challengeid):
        self.challengeid = challengeid
        self.sessiondata = os.path.join(SessionPath, challengeid + '.' + 'json')

        config.read(configsessions)
        self.durday = int(config[self.challengeid]['Duration'])
        self.reps = config[self.challengeid]['Repetitions']
        self.sets = config[self.challengeid]['Sets']

    # get values from the <challenge-id>.json and store them as a dict.
    # if no <challenge-id>.json exist then call challstart()
    def handler(self):
        try:
            with open(self.sessiondata, "r") as outfile:
                self.sessiondict = json.load(outfile)
                self.startday = datetime.strptime(self.sessiondict["Start"], '%Y-%m-%d').date()
                self.fin = datetime.strptime(self.sessiondict["Finish"], '%Y-%m-%d').date()
                # with the vars from <challenge-id>.json call challstatus
                self.challstatus()
        except OSError:
            self.challstart()

    # create an new <challenge-id>.json with the current date as startdate and the enddate from duration parsed out
    # of the Session.ini
    def challstart(self):
        startday = date.today()
        plusdays = timedelta(days=self.durday)
        fin = startday + plusdays
        print("...Start new Challenge...")
        print("ID:", self.challengeid)
        sessiondict = {"Start": str(startday), "Finish": str(fin)}
        # file .json gets created and opened as the variable `outfile`
        with open(self.sessiondata, 'w') as outfile:
            # `sessiondict` sample gets written into the .json -- sort_keys, indent are optional and used for
            # pretty-write
            json.dump(sessiondict, outfile, sort_keys=True, indent=4)
        # call handler again to read the vars
        self.handler()

    # show the status/values of the current challenge
    def challstatus(self):
        print("\nID:", self.challengeid)
        print("Current Challenge Status")
        print("========================")
        print("Start Challenge:", self.startday)
        print("Duration:", self.durday, "Days")
        print("Accomplished at:", self.fin)
        print("Repetition", self.reps)
        print("Sets:", self.sets)
        print("------------------------")
        today = date.today()
        daysleft = self.fin - today
        print("Days left:", daysleft.days)
        if daysleft.days < 0:
            print("Congratulations")
            # TODO: last day und volume 100% -> congrats -> start new/remove `session.json` + Section in `Session.ini`?
        startvol = input("\nDo you want to track your progress? - Y(es) or N(o) [default: Y] \n")
        if len(startvol) == 0:
            startvol = "Y"
        if startvol == "Y" or startvol == "y" or startvol == "yes" or startvol == "Yes":
            # for track the progress call volume()
            self.volume()
        elif startvol == "N" or startvol == "n" or startvol == "no" or startvol == "No":
            # go back to main-menue (Setup-Class)
            Set.handler()
        else:
            # function call itself when input is not clear Yes or No
            self.challstatus()

    # track progress. input of Repetitions/Sets. if at the same day progress been tracked then add the input to this
    # value. if the <challenge-id>.json not contain the present date then create the key+value for the parsed dict.
    def volume(self):
        today = str(date.today())
        while True:
            try:
                done = int(input("\nHow many Repetitions/Sets have you done? "))
            except ValueError:
                print("Sorry, please give a Number.")
                continue
            else:
                break

        if today in self.sessiondict:
            self.sessiondict[today] = int(self.sessiondict[today]) + done
            print("\nTotal done today: ", self.sessiondict[today])
        else:
            self.sessiondict[today] = done
            print("\nTotal done today: ", self.sessiondict[today])

        # File .json gets created and opened as the variable `outfile`.
        with open(self.sessiondata, 'w') as outfile:  #
            # `sessiondict` sample gets written into the .json -- sort_keys, indent are optional and used for
            # pretty-write
            json.dump(self.sessiondict, outfile, sort_keys=True, indent=4)


if __name__ == "__main__":
    # Initial Setup creates a configfile in the Script-Dir. This `config.ini` contains the path for storing the
    # progress -- `SessionPath`.
    # In this `SessionPath` the script writes a `Session.ini` (var: configsessions) with settings of all started
    # challenges. In the same folder each challenge will create a file called `<Challenge-ID>.json`,
    # where start-,enddate and the daily progress will be stored.

    # Common DIRs; uncomment for debug if your bash/shell get wrong DIRs
    cwd = os.getcwd()
    # print("Current Working DIR:", cwd)
    wdir = os.path.dirname(os.path.realpath(__file__))
    # print("Script DIR:", wdir)

    configfile = os.path.join(wdir, "config" + "." + "ini")
    print("Config:", configfile)

    config = configparser.ConfigParser()

    # test if the config.ini exists. if yes, it get parsed. if not, create and ask for the value/path to store the
    # progress-data
    try:
        with open(configfile, "r") as conf:
            print("\n...Read Config...")
            config.read(configfile)
            SessionPath = config.get('Customization', 'Path')
    except OSError:
        print("\nInitial Setup")
        print("=============")
        SessionPath = input("Enter the path where you want to save your progress: \n")
        config['Customization'] = {'Path': SessionPath}
        with open(configfile, 'w') as conf:
            config.write(conf)

    configsessions = os.path.join(SessionPath, 'Session' + '.' + 'ini')
    print("Session-Conf:", configsessions)

    Set = Setup(configsessions)
    Set.handler()